#include "CalculatorParserHandler.hh"

CalculatorParserHandler::CalculatorParserHandler(std::string location): _location(location) {}

void CalculatorParserHandler::enterMain(CalculatorParser::MainContext *ctx) {

    std::cout << "Entered in MAIN" << std::endl;
}

void CalculatorParserHandler::exitMain(CalculatorParser::MainContext *ctx) {
    std::cout << "Exited from MAIN" << std::endl;
}

void CalculatorParserHandler::enterExpr(CalculatorParser::ExprContext *ctx) {
    std::cout << "Entered in EXPR" << std::endl;
}

void CalculatorParserHandler::exitExpr(CalculatorParser::ExprContext *ctx) {
    std::cout << "Exited from EXPR" << std::endl;
}