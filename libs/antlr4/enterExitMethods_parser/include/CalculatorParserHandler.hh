#pragma once

#include "CalculatorBaseListener.h"
#include <string>

using namespace antlr4;


class CalculatorParserHandler : public CalculatorBaseListener {
public:
explicit CalculatorParserHandler(std::string location);

  ~CalculatorParserHandler() override = default;

		// I want to override only these 2 enter methods
  virtual void enterMain(CalculatorParser::MainContext *ctx) override;
  virtual void enterExpr(CalculatorParser::ExprContext *ctx) override;

  virtual void exitMain(CalculatorParser::MainContext *ctx) override;
  virtual void exitExpr(CalculatorParser::ExprContext *ctx) override;



private:
		std::string _location;
};

