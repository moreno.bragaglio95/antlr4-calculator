
// Generated from Calculator.g4 by ANTLR 4.7.1

#pragma once


#include "antlr4-runtime.h"
#include "CalculatorParser.h"


/**
 * This interface defines an abstract listener for a parse tree produced by CalculatorParser.
 */
class  CalculatorListener : public antlr4::tree::ParseTreeListener {
public:

  virtual void enterMain(CalculatorParser::MainContext *ctx) = 0;
  virtual void exitMain(CalculatorParser::MainContext *ctx) = 0;

  virtual void enterExpr(CalculatorParser::ExprContext *ctx) = 0;
  virtual void exitExpr(CalculatorParser::ExprContext *ctx) = 0;


};

