# generate Listener from the grammar Calculator.g4
rm -rf libs/antlr4/generatedFromCMake/*
java -jar /usr/local/lib/antlr-4.7.1-complete.jar -Dlanguage=Cpp -o libs/antlr4/generatedFromCMake/ Calculator.g4


# compile and run
rm -rf build/
mkdir build
cd build
cmake ..
# the following "make -j4" return errors because the build directory does not contains the ANTLR repository (that will be cloned with this "make -j4")
make -j4
# the following "make -j4" has the ANTLR repo cloned so it does not return error
make -j4
./antlr4-tutorial


