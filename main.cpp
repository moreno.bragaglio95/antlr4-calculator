#include <iostream>
#include <fstream>
#include <string>

#include "antlr4-runtime.h"
#include "libs/antlr4/generatedFromCMake/CalculatorLexer.h"
#include "libs/antlr4/generatedFromCMake/CalculatorParser.h"
#include "libs/antlr4/enterExitMethods_parser/include/CalculatorParserHandler.hh"

int main(int argc, const char* argv[]) {
    CalculatorParserHandler listener("main var");
    std::fstream inFile;
    inFile.open("../input.txt",std::ios::in);
    if(inFile.is_open()) //checking whether the file is open
    {
        std::string tp;
        while(getline(inFile, tp)){ //read data from file object and put it into string.
            std::cout << tp << "\n"; //print the data of the string
            antlr4::ANTLRInputStream input(tp);
            CalculatorLexer lexer(&input);
            antlr4::CommonTokenStream tokens(&lexer);
            CalculatorParser parser(&tokens);
            tree::ParseTree *treeCalc = parser.main();
            // std::cout << treeLocDec->toStringTree(&parserPrecLocDec) << "\n\n\n";
            tree::ParseTreeWalker::DEFAULT.walk(&listener, treeCalc);

        }
        inFile.close(); //close the file object.
    }
    else std::cout << "Error!!" << std::endl;


}

